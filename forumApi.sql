-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : ven. 15 jan. 2021 à 18:51
-- Version du serveur :  10.3.25-MariaDB-0+deb10u1
-- Version de PHP : 7.3.19-1~deb10u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `forumApi`
--

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `category`
--

INSERT INTO `category` (`id`, `title`, `icon`) VALUES
(35, 'Linux', '#000000'),
(36, 'Windows', '#000000'),
(37, 'Matériel informatique', '#000000'),
(38, 'Internet, Web et réseaux', '#000000'),
(39, 'Objets connectés, mobilité, téléphonie', '#000000');

-- --------------------------------------------------------

--
-- Structure de la table `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `post`
--

INSERT INTO `post` (`id`, `author_id`, `category_id`, `state_id`, `title`, `slug`, `content`, `created_at`, `updated_at`) VALUES
(61, 11, 35, 2, 'Windows ne boot plus après suppression partition ubuntu', 'windows-ne-boot-plus-apres-suppression-partition-ubuntu-6001c1900ddc3', 'Bonjour et d\'avance merci pour votre aide.\n\nJ\'ai acheté d\'occasion un pc mobile , LENOVO THINKPAD W540 avec 2 systèmes d\'exploitation, WINDOWS 10 64 BIT et UBUNTU .\n\nComme je n\'utilise pas LINUX que je ne connais pas, j\'ai effacé sa partition sur le disque croyant booter directement sur WINDOWS.\nDepuis, impossible de booter, je tombe systématiquement sur le message: no such partition/entering rescue mode/grub rescue.\n\nAvant d\'effacer la partition j\'ai effectué une sauvegarde IMAGE et creé un disque de réparation mais ça n\'a servi a rien.\n', '2021-01-15 17:17:22', '2021-01-15 17:23:44'),
(62, 20, 35, 1, 'Ecran noir impossible d\'ouvrir xubuntu 18.04', 'ecran-noir-impossible-d-ouvrir-xubuntu-18-04-6001c1ced2bc7', 'Bonjour\nje suis completement bloqué avec mon pc portable acer aspire ES1.\nje pense que la raison est qu\'il na plus de place sur le disque dur car j\'ai fait un rapport sur grub qui me donne entre autres : systeme fichier : /dev/mmcblkop2.\nje ne sais pas comment liberer de la place car je n\'ai pas accés à rien.\nje ne connais pas grand chose en informatique.\nj\'attends votre aide\nmerci d\'avance', '2021-01-15 17:24:46', NULL),
(63, 20, 35, 3, 'Creation serveur FTP Linux', 'creation-serveur-ftp-linux-6001c257151e2', 'Bonjour à tous,\nJe suis nouveau sur le forum et malgré les explications de Malekal sur le sujet des serveurs FTP j\'aurais besoin d\'un petit coup de main :)\n\nPour un projet d\'étude je dois créer un serveur FTPS sous Linux sur lequel les utilisateurs locaux (sous Windows) de mon entreprise fictive pourront récupérer des fichiers, ainsi que des utilisateurs externes à l\'entreprise.\n\nJ\'ai donc configuré un serveur NTP, puis Kerberos, puis SAMBA, puis WINBIND avant d\'arriver sur la mise en place du serveur FTPS.\n\nLes répertoires utilisateurs seront situés dans le répertoire /share qui sera lui même partagé avec samba.. Ce répertoire devra appartenir au root et au groupe gg_partage_ftp de l’AD. Le gid sera positionné afin que les sous-éléments créés dans le dossier appartiennent au groupe gg_partage_ftp.\n\nJ\'ai créé mon dossier de partage /share et maintenant j\'en au stade ou j\'aimerais ajouter mes utilisateurs appartement au GG_Partage_FTP sous Windows avec la commande suivante :\nchown root.\'COMPRA\\GG_Partage_FTP\' /share && chmod 2771 /share.\n\nJe ne sais pas si je suis très clair mais je ne sais pas comment expliquer les différentes étapes de ma manipulation. Tout avant a correctement fonctionné et j\'ai point joindre mon domaine et mon DNS sans problème.\n\nMerci d\'avance pour vos réponses !', '2021-01-15 17:27:03', NULL),
(65, 20, 35, 1, 'Question sur linux', 'question-sur-linux-6001c5583f209', 'Faites-nous grâce de vos étourdies les méduses, etc. Occupe-toi de la santé à boire. Abandon de toutes les épreuves qu\'elle avait toujours sa main, qu\'elle posait. Prétendez-vous organiser un peuple de débardeurs, que dominait le bras gigantesque d\'une population grandissante, et l\'honneur. Impassible comme à son compagnon, et pâlit de joie à l\'appel que pour rire des ordonnateurs. Simplement, ils s\'attribuaient beaucoup d\'importance, car ses pieds ont disparu. Avaient-elles été habitées par un nombre de cas, qu\'un des trous de brume. Causons donc, ma petite fille chérie, ses manières devinrent plus familières.\nBasque avait supprimé les bouteilles, on s\'attache à faire voir que, si l\'aubergiste s\'était, élevé. Travaillons à empêcher la jonction des contingents ennemis, afin de constituer la science.', '2021-01-15 17:39:52', NULL),
(66, 20, 35, 1, 'Who was your favorite teacher and why?', 'who-was-your-favorite-teacher-and-why-6001c5b8e536c', 'One advanced diverted domestic sex repeated bringing you old. Possible procured her trifling laughter thoughts property she met way. Companions shy had solicitude favourable own. Which could saw guest man now heard but. Lasted my coming uneasy marked so should. Gravity letters it amongst herself dearest an windows by. Wooded ladies she basket season age her uneasy saw. Discourse unwilling am no described dejection incommode no listening of. Before nature his parish boy. ', '2021-01-15 17:41:28', NULL),
(67, 20, 35, 1, 'What story does your family always tell about you?', 'what-story-does-your-family-always-tell-about-you-6001c5ce06adc', 'Folly words widow one downs few age every seven. If miss part by fact he park just shew. Discovered had get considered projection who favourable. Necessary up knowledge it tolerably. Unwilling departure education is be dashwoods or an. Use off agreeable law unwilling sir deficient curiosity instantly. Easy mind life fact with see has bore ten. Parish any chatty can elinor direct for former. Up as meant widow equal an share least. ', '2021-01-15 17:41:50', NULL),
(68, 20, 35, 1, 'If you lost all of your possessions but one, what would you want it to be?', 'if-you-lost-all-of-your-possessions-but-one-what-would-you-want-it-to-be-6001c5df04b19', 'Village did removed enjoyed explain nor ham saw calling talking. Securing as informed declared or margaret. Joy horrible moreover man feelings own shy. Request norland neither mistake for yet. Between the for morning assured country believe. On even feet time have an no at. Relation so in confined smallest children unpacked delicate. Why sir end believe uncivil respect. Always get adieus nature day course for common. My little garret repair to desire he esteem. ', '2021-01-15 17:42:07', NULL),
(69, 20, 35, 1, 'What\'s the worst movie you\'ve ever seen?', 'what-s-the-worst-movie-you-ve-ever-seen-6001c5f584b74', 'Had denoting properly jointure you occasion directly raillery. In said to of poor full be post face snug. Introduced imprudence see say unpleasing devonshire acceptance son. Exeter longer wisdom gay nor design age. Am weather to entered norland no in showing service. Nor repeated speaking shy appetite. Excited it hastily an pasture it observe. Snug hand how dare here too. ', '2021-01-15 17:42:29', NULL),
(70, 20, 35, 1, 'Who are some of your heroes?', 'who-are-some-of-your-heroes-6001c611db24c', 'You disposal strongly quitting his endeavor two settling him. Manners ham him hearted hundred expense. Get open game him what hour more part. Adapted as smiling of females oh me journey exposed concern. Met come add cold calm rose mile what. Tiled manor court at built by place fanny. Discretion at be an so decisively especially. Exeter itself object matter if on mr in. ', '2021-01-15 17:42:57', NULL),
(71, 20, 35, 1, 'What\'s the best thing you got from one of your parents?', 'what-s-the-best-thing-you-got-from-one-of-your-parents-6001c63825367', 'Article nor prepare chicken you him now. Shy merits say advice ten before lovers innate add. She cordially behaviour can attempted estimable. Trees delay fancy noise manor do as an small. Felicity now law securing breeding likewise extended and. Roused either who favour why ham. ', '2021-01-15 17:43:36', NULL),
(72, 20, 35, 1, 'What dumb accomplishment are you most proud of?', 'what-dumb-accomplishment-are-you-most-proud-of-6001c65a29909', 'Effect if in up no depend seemed. Ecstatic elegance gay but disposed. We me rent been part what. An concluded sportsman offending so provision mr education. Bed uncommonly his discovered for estimating far. Equally he minutes my hastily. Up hung mr we give rest half. Painful so he an comfort is manners. ', '2021-01-15 17:44:10', NULL),
(73, 20, 35, 1, 'If you had to change your name, what would you change it to?', 'if-you-had-to-change-your-name-what-would-you-change-it-to-6001c66eceffa', 'Effect if in up no depend seemed. Ecstatic elegance gay but disposed. We me rent been part what. An concluded sportsman offending so provision mr education. Bed uncommonly his discovered for estimating far. Equally he minutes my hastily. Up hung mr we give rest half. Painful so he an comfort is manners. ', '2021-01-15 17:44:30', NULL),
(74, 11, 36, 1, 'What\'s the best thing you got from one of your parents?', 'what-s-the-best-thing-you-got-from-one-of-your-parents-6001c6c55e342', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.', '2021-01-15 17:45:57', NULL),
(75, 11, 36, 1, 'What are you interested in that most people haven\'t heard of?', 'what-are-you-interested-in-that-most-people-haven-t-heard-of-6001c6d2778ac', 'Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum.', '2021-01-15 17:46:10', NULL),
(76, 11, 36, 1, 'When was the last time you changed your opinion about something major?', 'when-was-the-last-time-you-changed-your-opinion-about-something-major-6001c6e5e0b4d', 'Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus.', '2021-01-15 17:46:29', NULL),
(77, 11, 36, 1, 'What\'s your favorite book?', 'what-s-your-favorite-book-6001c70c7ccc0', 'Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis', '2021-01-15 17:47:08', NULL),
(78, 11, 36, 1, 'If you lost all of your possessions but one, what would you want it to be?', 'if-you-lost-all-of-your-possessions-but-one-what-would-you-want-it-to-be-6001c71e9a361', 'Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa.', '2021-01-15 17:47:26', NULL),
(79, 11, 36, 1, 'What was the best compliment you\'ve ever received?', 'what-was-the-best-compliment-you-ve-ever-received-6001c7530c069', 'Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum.', '2021-01-15 17:48:19', NULL),
(80, 11, 37, 1, 'What languages do you speak?', 'what-languages-do-you-speak-6001c7a061eb9', 'Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet.', '2021-01-15 17:49:36', NULL),
(81, 11, 37, 1, 'What\'s the best piece of advice you ever received?', 'what-s-the-best-piece-of-advice-you-ever-received-6001c7b252876', 'Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui.', '2021-01-15 17:49:54', NULL),
(82, 11, 37, 1, 'What\'s your worst habit?', 'what-s-your-worst-habit-6001c7c7a0bd0', 'Pellentesque ut neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In dui magna, posuere eget, vestibulum et, tempor auctor, justo. In ac felis quis tortor malesuada pretium. Pellentesque auctor neque nec urna. Proin sapien ipsum, porta a, auctor quis, euismod ut, mi. Aenean viverra rhoncus pede.', '2021-01-15 17:50:15', NULL),
(83, 11, 37, 1, 'What talent would you show off in a talent show?', 'what-talent-would-you-show-off-in-a-talent-show-6001c7e6379fb', 'Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis diam.', '2021-01-15 17:50:46', NULL),
(84, 23, 38, 1, 'What\'s one of your favorite comfort foods?', 'what-s-one-of-your-favorite-comfort-foods-6001c80b9f750', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut non enim eleifend felis pretium feugiat. Vivamus quis mi. Phasellus a est. Phasellus magna. In hac habitasse platea dictumst. Curabitur at lacus ac velit ornare lobortis. Curabitur a felis in nunc fringilla tristique. Morbi mattis ullamcorper velit. Phasellus gravida semper nisi. Nullam vel sem. Pellentesque libero tortor, tincidunt et, tincidunt eget, semper nec, quam. Sed hendrerit. Morbi ac felis. Nunc egestas, augue at pellentesque laoreet, felis eros vehicula leo, at malesuada velit leo quis pede. Donec interdum, metus et hendrerit aliquet, dolor diam sagittis ligula, eget egestas libero turpis vel mi. Nunc nulla.', '2021-01-15 17:51:23', NULL),
(85, 23, 38, 1, 'Pirates or ninjas?', 'pirates-or-ninjas-6001c81ba5dbb', 'Praesent egestas neque eu enim. In hac habitasse platea dictumst. Fusce a quam. Etiam ut purus mattis mauris sodales aliquam. Curabitur nisi. Quisque malesuada placerat nisl. Nam ipsum risus, rutrum vitae, vestibulum eu, molestie vel, lacus. Sed augue ipsum, egestas nec, vestibulum et, malesuada adipiscing, dui. Vestibulum facilisis, purus nec pulvinar iaculis, ligula mi congue nunc, vitae euismod ligula urna in dolor. Mauris sollicitudin fermentum libero. Praesent nonummy mi in odio. Nunc interdum lacus sit amet orci. Vestibulum rutrum, mi nec elementum vehicula, eros quam gravida nisl, id fringilla neque ante vel mi. Morbi mollis tellus ac sapien. Phasellus volutpat, metus eget egestas mollis, lacus lacus blandit dui, id egestas quam mauris ut lacus. Fusce vel dui.', '2021-01-15 17:51:39', NULL),
(86, 23, 39, 1, 'J\'ai un probleme avec mon Apple Watch', 'j-ai-un-probleme-avec-mon-apple-watch-6001c8828c603', 'D’abord, vous savez que la montre d’Apple, en version Cellular (celle qui contient un e-sim et qui permet de téléphoner ou recevoir des données sans iPhone), est une catastrophe d’autonomie dès qu’on la porte éloigné dudit iPhone et hors de portée de votre réseau wi-fi, ce qui est tout de même le but de cette montre.', '2021-01-15 17:53:22', NULL),
(87, 23, 39, 3, 'changement de compte sur android', 'changement-de-compte-sur-android-6001c94fa7eb5', 'Bonjour à tous,\n\n(Edit je ne dois pas être dans le bon sous-forum! alors qu\'il me semble bien avoir choisi celui android du coup je ne sais comment déplacer ce message!)\n\nJe poste cette demande d\'aide afin d\'avoir vos avis,\nJ\'avais un compte google sur mon smartphone et ait décidé de supprimer du mobile ce compte, du coup sur ce téléphone je n\'ai maintenant plus accès à l\'ancien gmail relié à ce compte ni à l\'ancien play store lié à ce compte google\n\nA la place j\'ai mis un autre compte google, du coup en boîte gmail c\'est bien ce que je veux mais lorsque je vais dans le play store -> mes jeux et applications\n\ndans la rubrique mise à jour et dans celle applications installées ne sont plus affichées la cinquantaine d\'applis que j\'avais (sans doute logique puisque les applis avaient été téléchargées via l\'ancien compte play store et étaient quelque part reliées à ce même ancien compte)', '2021-01-15 17:56:47', NULL),
(88, 23, 39, 1, 'Des conseils pour un nouveau smartphone? (budget 300€)', 'des-conseils-pour-un-nouveau-smartphone-budget-300EUR-6001cabdd48f3', 'Hello!\nJ\'y connais pas grand chose en smartphone (j\'utilise un vieux samsung depuis 7-8 ans je pense? Je sais même pas de quel modèle il s\'agit ) et j\'ai check un peu les tests sur divers sites mais je suis un peu perdu...\n\nLes raisons qui me poussent à changer de téléphones sont:\n1-Prendre des photos de bonne qualité, même sur écran d\'ordinateur,\n2-retrouver un confort d\'utilisation, vu l\'âge de mon téléphone vous vous doutez bien qu\'il met beaucoup de temps à ouvrir chaque appli, naviguer sur internet est quasi impossible (chrome se ferme automatiquement sur la plupart des sites, google, wikipedia mais le reste en général c\'est niet)', '2021-01-15 18:02:53', NULL),
(89, 20, 36, 1, 'Plantage Windows Récurent', 'plantage-windows-recurent-6001cb670d6d4', 'Bonjour tout le monde,\n\nCela fait maintenant plus d\'un an et demi que je possède mon MSI GP73 Leopard 8RE et voilà maintenant 2-3 mois que j\'ai un problème assez récurent.\nMise en contexte : J\'utilise mon ordinateur normalement, je regarde la tv dessus ou autre et l\'ordinateur plante. Un écran bleu s\'affiche en me disant que Windows a rencontré un problème et qu\'il doit redémarrer. Lorsqu\'il redémarre, il reste bloqué sur le logo de démarrage MSI, je dois donc l\'éteindre en force et quand je le redémarre, il ne démarre pas sur le bon disque dur, m\'affiche donc le menu \"startup.nsh\". Je sais que ce message s\'affiche lorsque l\'ordinateur ne démarre pas sur le bon disque dur, mon Windows 10 étant installé sur le SSD, j\'accède donc au BIOS pour remettre mon SSD comme disque dur de démarrage.', '2021-01-15 18:05:43', NULL),
(90, 20, 36, 3, 'Installation de Windows 10 sur un nouveau SSD', 'installation-de-windows-10-sur-un-nouveau-ssd-6001cb819a486', 'Bonjour,\n\nJe me suis acheté un SSD d\'1To dans l\'optique d\'y installer proprement Windows 10 ainsi que des jeux. J\'aimerais donc pouvoir d\'abord installer un nouveau Windows 10 sur le SSD, puis récupérer sur le HDD (le C: actuel) tous les fichiers que je souhaite conserver et enfin formater le HDD avec l\'ancien Windows 10 dessus\n', '2021-01-15 18:06:09', NULL),
(91, 20, 37, 1, 'BSoD à répétition, probable soucis de carte graphique', 'bsod-a-repetition-probable-soucis-de-carte-graphique-6001cbeb7c629', 'Bonjour,\nIl s\'agit de mon deuxième post aujourd\'hui, sur un autre soucis concernant mon ASUS R751l.\nJe l\'ai depuis environ 2 ans et demi, et il a toujours fonctionné du tonnerre! Je faisais même tourner des jeux dessus (Skyrim, Age of Empire 3, Sins of a Solar Empire, et autres), il fonctionnait parfaitement, fluide et sans bug (quelques freezes, rarement, sur Skyrim, mais le pc se remettait d\'aplomb en quelques secondes, et le jeu reprenait comme si de rien n\'était). ', '2021-01-15 18:07:55', NULL),
(92, 20, 37, 3, 'Ventilateur de boitier qui ne tourne pas', 'ventilateur-de-boitier-qui-ne-tourne-pas-6001cc202b3ea', 'Bonjour,\n\nAlors voila il y a quelque jours j\'ai remarquer que mes ventilateurs rgb ne tourner plus, ils on un départ quand j\'allume mon pc et se bloque tout de suite après.\n\nJ\'ai d\'abord vérifier si il tourner manuellement, il tourne.\nLes leds des ventilateurs sont allumer donc je me suis dis c\'est pas un problème d\'alimentation.\nAprès j\'ai vérifier le bios sa a pas l\'air de venir de la non plus car la t° système est afficher et le nombres de rpm des ventilateurs affiche 0.\n\nEst que quelqu\'un pourrais m\'aider s\'il vous plait ? d\'ou peut venir le problème ?', '2021-01-15 18:08:48', NULL),
(93, 20, 38, 1, 'Impossible de me connecter a facebook', 'impossible-de-me-connecter-a-facebook-6001cc7a65b68', 'Bonjour,\nJe n\'arrive plus a me connecter a facebook. Comment faire?', '2021-01-15 18:10:18', NULL),
(94, 23, 36, 2, 'Windows defender help', 'windows-defender-help-6001d1a574394', 'Bonjour\nEt permettez moi de vous présenter tous mes meilleurs voeux pour l\'année 2021.\n\nCe matin au démarrage de mon PC un probleme est apparut avec l\'antivirus Windows Defender je n\'arrive plus à activer la protections des virus et menaces, pourtant tous fonctionnait bien hier soir avant l\'arrêt du pc .Quand je click sur redémarrer maintenant un message erreur inattendue apparait.\nMerci pour votre aide.', '2021-01-15 18:18:35', '2021-01-15 18:32:21');

-- --------------------------------------------------------

--
-- Structure de la table `post_tag`
--

CREATE TABLE `post_tag` (
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `post_tag`
--

INSERT INTO `post_tag` (`post_id`, `tag_id`) VALUES
(61, 53),
(61, 54),
(61, 55),
(63, 53),
(63, 56),
(63, 57),
(63, 58),
(66, 63),
(66, 64),
(66, 65),
(70, 61),
(70, 62),
(70, 66),
(74, 62),
(74, 67),
(74, 68),
(79, 58),
(79, 65),
(79, 69),
(83, 70),
(83, 71),
(86, 72),
(86, 73),
(86, 74),
(86, 75),
(87, 76),
(87, 77),
(87, 78),
(87, 79),
(87, 80),
(87, 81),
(87, 82),
(87, 83),
(87, 84),
(87, 85),
(87, 86),
(87, 87),
(89, 88),
(89, 89),
(89, 90),
(89, 91),
(89, 92),
(91, 93),
(91, 94),
(91, 95),
(91, 96),
(92, 97),
(92, 98),
(92, 99),
(93, 53),
(93, 75),
(93, 77),
(93, 100),
(94, 56),
(94, 62),
(94, 75),
(94, 77),
(94, 88);

-- --------------------------------------------------------

--
-- Structure de la table `response`
--

CREATE TABLE `response` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `response`
--

INSERT INTO `response` (`id`, `post_id`, `author_id`, `content`, `created_at`, `updated_at`) VALUES
(86, 61, 20, 'Bonsoir,\n\nDepuis un ordinateur fonctionnel, utilise Media Creation pour créer une clé USB d\'installation.\nUne fois démarré dessus, vas dans \"Réparer ordinateur\".\nTape diskpart et entrée, donne le résultat de la commande suivante: list disk\n', '2021-01-15 17:20:53', NULL),
(87, 61, 11, 'C\'est bon j\'ai résolu mon probleme!', '2021-01-15 17:23:12', NULL),
(89, 63, 11, 'Salut,\n\nC\'est obligatoire le SAMBA ?\nCa fait longtemps que j\'en ai pas trop fait, parce que tu peux monter un lecteur réseau sur Windows qui attaque un FTP.', '2021-01-15 17:28:02', NULL),
(90, 63, 20, 'Merci pour ton retour.\n\nMalheureusement oui, le SAMBA est obligatoire dans les consignes...\nJ\'avais vu qu\'il était possible de monter un lecteur réseau mais pas sûr que ça plaise au jury :D\n\nJ\'ai essayé ta ligne de commande pour le chown mais sans succès.\nPar contre j\'ai réessayé la mienne et elle semble passer après avoir remplacé un \"\\\" par un \"/\" entre \"COMPRA\" et \"gg\", du moins je n\'ai pas de retour négatif du terminal...', '2021-01-15 17:28:13', NULL),
(91, 63, 11, 'Je ne vois pas d\'erreur dans ton script.\nFaut voir si tu as des erreurs quand tu l\'utilises.\n\nTiens avec une vérif sur les caractères spéciaux et la création du dossier, je sais plus si adduser le créé.', '2021-01-15 17:28:36', NULL),
(92, 63, 20, 'Merci ta réponse.\n\nMalheureusement, comme dit mon script n\'a pas l\'air de fonctionner car lorsque je veux faire la création du mot de passe de ma façon ou de la tienne il m\'indique que l\'utilisateur n\'existe pas... Alors que je viens de le lancer et que mon user n\'a pas de caractère spéciaux.\n\nJe ne comprends pas.', '2021-01-15 17:28:45', NULL),
(93, 63, 23, 'Il y a des erreurs à l\'exécution du script ?\nVérifie la liste des utilisateurs : cat /etc/passwd', '2021-01-15 17:29:04', NULL),
(94, 63, 20, 'C\'est ça le problème c\'est qu\'il ne m\'indique pas d\'erreurs mais ne créé pas mes utilisateurs Alors que quand j\'utilise la commande adduser cela fonctionne.\n\nBon ce n\'est pas grave, je ferais manuellement.\n\nPar contre aurais-tu une idée d\'une commande permettant d\'importer les utilisateurs de l\'AD de mon Windows Serveur vers mon serveur Linux pour ne pas avoir à tout créer manuellement justement ?', '2021-01-15 17:29:16', NULL),
(95, 94, 20, 'Quel est exactement ta version?\nLa version stable::Windows 10Version 20H2(19042.685)', '2021-01-15 18:21:03', NULL),
(96, 94, 23, 'Bonsoir\n\nMerci pour ton aide , j\'ai fait ce dont tu m\'as demandé pour la version de Windows c\'est bien la version 20H2(19042.685)\npour la mise à jour j\'ai suivi tes commentaires mais aucune mise a jour .', '2021-01-15 18:21:21', NULL),
(97, 94, 20, 'Re.\n\nSi ce n\'est pas réglé:\n\nTu mets le rapport Scan ZHPSuite', '2021-01-15 18:21:37', NULL),
(98, 94, 23, 'Ok je m\'en occupe , je pense que mon pc est infecté car je n\'arrive plus à lancer Malwarebytes sa me met sa', '2021-01-15 18:21:46', NULL),
(99, 94, 20, 'Bonsoir\n\nVoici le lien : https://www.cjoint.com/c/KAmszNmT5lh', '2021-01-15 18:24:05', NULL),
(100, 94, 23, 'salut,\n\nas tu une version de malwarebytes premium ?\n\nle pc est infecté', '2021-01-15 18:24:21', NULL),
(101, 94, 20, 'Bonsoir\n\nJ\'ai posté ou tu m\'as dit c\'est en cours de désinfection , la version de malwarebytes est 3.7.1.2839 ', '2021-01-15 18:24:32', NULL),
(102, 94, 20, 'Si c\'est la version Malwarebytes Premium ou une période d\'essai avec la version Malwarebytes Premium ça peut désactiver une partie des fonctions de Windows defender', '2021-01-15 18:24:52', NULL),
(103, 94, 23, 'Moi j\'ai la dernière version de Malwarebytes Gratuit 4.3.0 donc la Protection en temps réel Malwarebytes Premium en bas à droite est désactivé\n\nSinon avec cette veille version Malwarebytes version 3.7.1 tu as peut être des dysfonctionnements. ,désinstalle la ,redémarre le PC ,puis essai de nouveau de réactiver tout le Windows defender', '2021-01-15 18:25:01', NULL),
(104, 94, 23, 'je ne pense pas que se soit la premium\n\nEn tous les cas Merci pour ton aide ', '2021-01-15 18:25:13', NULL),
(105, 94, 20, 'Sinon avec cette veille version Malwarebytes version 3.7.1 tu as peut être des dysfonctionnements. ,désinstalle la ,redémarre le PC ,puis essai de nouveau de réactiver tout le Windows defender', '2021-01-15 18:25:26', NULL),
(106, 92, 23, 'Bonjour, Il suffit de changer de ventilo', '2021-01-15 18:26:23', NULL),
(107, 92, 20, 'Oui mais je ne m\'y connais pas en informatique', '2021-01-15 18:26:36', NULL),
(108, 92, 23, 'C\'est tres simple, aussi facile que de changer une ampoule', '2021-01-15 18:26:50', NULL),
(109, 87, 20, 'Salut, il faut que tu contact google!!!', '2021-01-15 18:27:32', NULL),
(110, 90, 20, 'Je viens de tout reinstaller et j\'ai toujour la même erreur', '2021-01-15 18:28:12', NULL),
(111, 90, 20, 'S\'il vous plait, j\'ai besoin d\'aide', '2021-01-15 18:28:22', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `state`
--

CREATE TABLE `state` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `state`
--

INSERT INTO `state` (`id`, `title`, `icon`) VALUES
(1, 'NEW', '#DC3545'),
(2, 'CLOSE', '#28A745'),
(3, 'EN COURS', '#17A2B8');

-- --------------------------------------------------------

--
-- Structure de la table `tag`
--

CREATE TABLE `tag` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `tag`
--

INSERT INTO `tag` (`id`, `title`) VALUES
(65, 'aleatoirement'),
(60, 'amerique'),
(76, 'Android'),
(72, 'Apple'),
(96, 'Asus'),
(78, 'beacoup'),
(93, 'BSOD'),
(94, 'CG'),
(77, 'Compte'),
(63, 'contenu'),
(79, 'de tags'),
(90, 'Disque dur'),
(54, 'dual-boot'),
(74, 'E-sim'),
(100, 'Facebook'),
(56, 'FTP'),
(57, 'FTPS'),
(64, 'generé'),
(85, 'Google'),
(86, 'GooglePlay'),
(66, 'HTML'),
(62, 'javascript'),
(84, 'l\'overflow'),
(99, 'leds'),
(55, 'lenovo'),
(68, 'man'),
(73, 'Montre'),
(92, 'MSI'),
(58, 'Partage'),
(87, 'PlayStore'),
(81, 'post'),
(82, 'pour'),
(59, 'recherche'),
(98, 'RPM'),
(95, 'Skyrim'),
(89, 'SSD'),
(91, 'startup'),
(67, 'super'),
(80, 'sur ce'),
(69, 'tag'),
(71, 'tag1'),
(70, 'tags'),
(61, 'test'),
(53, 'Ubuntu'),
(97, 'Ventilateurs'),
(83, 'voir'),
(75, 'Wifi'),
(88, 'windows');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:json)',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `email`, `roles`, `password`, `username`) VALUES
(11, 'admin@gmail.com', '[\"ROLE_USER\",\"ROLE_ADMIN\"]', '$argon2id$v=19$m=65536,t=4,p=1$sgR74CxdbBtNJ218EsqOoQ$LD/5EuxmyRdRZFsB3dykLrBF6pqqVXXCMS6/Pg6n3Sk', 'admin'),
(20, 'user1@randommail.com', '[\"ROLE_USER\"]', '$argon2id$v=19$m=65536,t=4,p=1$y+WsSsyxQgqs8eBe+kwYwQ$QIpDe2200t4jsR1AE680Mb6Rb7Zmq38Oilo5F6wDiVk', 'user1'),
(22, 'user3@mail.net', '[\"ROLE_USER\"]', '$argon2id$v=19$m=65536,t=4,p=1$5WSNtL5pQD+wSPCKWSc+HA$XbpW7/LFzddpPmoXDPLSdX1r3IV/IDaVwr2vZAPULA0', 'user3'),
(23, 'user2@mail.fr', '[\"ROLE_USER\"]', '$argon2id$v=19$m=65536,t=4,p=1$E/qjsy2ve3Y9MsAlFmZMEg$er6ZXUsc/+TAchQsW7BlxuDBzut481gI9ahcJ/6xATE', 'user2');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_5A8A6C8DF675F31B` (`author_id`),
  ADD KEY `IDX_5A8A6C8D12469DE2` (`category_id`),
  ADD KEY `IDX_5A8A6C8D5D83CC1` (`state_id`);

--
-- Index pour la table `post_tag`
--
ALTER TABLE `post_tag`
  ADD PRIMARY KEY (`post_id`,`tag_id`),
  ADD KEY `IDX_5ACE3AF04B89032C` (`post_id`),
  ADD KEY `IDX_5ACE3AF0BAD26311` (`tag_id`);

--
-- Index pour la table `response`
--
ALTER TABLE `response`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_3E7B0BFB4B89032C` (`post_id`),
  ADD KEY `IDX_3E7B0BFBF675F31B` (`author_id`);

--
-- Index pour la table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_389B7832B36786B` (`title`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`),
  ADD UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT pour la table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT pour la table `response`
--
ALTER TABLE `response`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;

--
-- AUTO_INCREMENT pour la table `state`
--
ALTER TABLE `state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `tag`
--
ALTER TABLE `tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `FK_5A8A6C8D12469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `FK_5A8A6C8D5D83CC1` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`),
  ADD CONSTRAINT `FK_5A8A6C8DF675F31B` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `post_tag`
--
ALTER TABLE `post_tag`
  ADD CONSTRAINT `FK_5ACE3AF04B89032C` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_5ACE3AF0BAD26311` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `response`
--
ALTER TABLE `response`
  ADD CONSTRAINT `FK_3E7B0BFB4B89032C` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`),
  ADD CONSTRAINT `FK_3E7B0BFBF675F31B` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
