<?php

// src/DataPersister

namespace App\DataPersister;

use App\Entity\Post;
use App\Entity\Tag;
use App\Entity\State;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\String\Slugger\SluggerInterface;
use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use Symfony\Component\Security\Core\Security;

/**
 *
 */
class PostDataPersister implements ContextAwareDataPersisterInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $_entityManager;

    /**
     * @param SluggerInterface
     */
    private $_slugger;

    /**
     * @param Request
     */
    private $_request;

    /**
     * @param Security
     */
    private $_security;

    public function __construct( EntityManagerInterface $entityManager, SluggerInterface $slugger, RequestStack $request, Security $security) {
        $this->_entityManager = $entityManager;
        $this->_slugger = $slugger;
        $this->_request = $request->getCurrentRequest();
        $this->_security = $security;
    }


    public function supports($data, array $context = []): bool
    {
        return $data instanceof Post;
    }

    /**
     * @param Post $data
     */
    public function persist($data, array $context = [])
    {
        // Creation du slug
        $data->setSlug($this->_slugger->slug(strtolower($data->getTitle())). '-' .uniqid());


        // Change le updatedAt si != de post (en cas d'update donc)
        if ($this->_request->getMethod() !== 'POST') {
            $data->setUpdatedAt(new \DateTime());
        }

        $tagRepository = $this->_entityManager->getRepository(Tag::class);
        foreach ($data->getTags() as $tag) {
            $t = $tagRepository->findOneByTitle($tag->getTitle());

            // Si le tag existe deja, on ne le persiste pas.
            if ($t !== null) {
                $data->removeTag($tag);
                $data->addTag($t);
            } else {
                $this->_entityManager->persist($tag);
            }
        }

        // Si nouveau post alors on set l'auteur avec l'utilisateur connecté
        if ($this->_request->getMethod() === 'POST') {
            $data->setAuthor($this->_security->getUser());
            $StateRepository = $this->_entityManager->getRepository(State::class);
            $state = $StateRepository->findOneByTitle("NEW");
            $data->setState($state);
        }

        $this->_entityManager->persist($data);
        $this->_entityManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function remove($data, array $context = [])
    {
        $this->_entityManager->remove($data);
        $this->_entityManager->flush();
    }
}