<?php

// src/DataPersister

namespace App\DataPersister;

use App\Entity\Response;
use App\Entity\Post;
use App\Entity\State;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\String\Slugger\SluggerInterface;
use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use Symfony\Component\Security\Core\Security;

/**
 *
 */
class ResponseDataPersister implements ContextAwareDataPersisterInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $_entityManager;

    /**
     * @param Request
     */
    private $_request;

    /**
     * @param Security
     */
    private $_security;

    public function __construct( EntityManagerInterface $entityManager, RequestStack $request, Security $security) {
        $this->_entityManager = $entityManager;
        $this->_request = $request->getCurrentRequest();
        $this->_security = $security;
    }


    public function supports($data, array $context = []): bool
    {
        return $data instanceof Response;
    }


    public function persist($data, array $context = [])
    {

        // Change le updatedAt si != de post (en cas d'update donc)
        if ($this->_request->getMethod() !== 'POST') {
            $data->setUpdatedAt(new \DateTime());

        }

        // Si nouvelle reponse alors on set l'auteur avec l'utilisateur connecté et on change le state du post
        if ($this->_request->getMethod() === 'POST') {
            $data->setAuthor($this->_security->getUser());
            $PostRepository = $this->_entityManager->getRepository(Post::class);
            $post =  $PostRepository->find($data->getPost());
            $StateRepository = $this->_entityManager->getRepository(State::class);
            $state = $StateRepository->findOneByTitle("EN COURS");
            $post->setState($state);
        }

        $this->_entityManager->persist($data);
        $this->_entityManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function remove($data, array $context = [])
    {

        $this->_entityManager->remove($data);
        $this->_entityManager->flush();

    }
}