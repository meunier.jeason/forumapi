<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Repository\PostRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ApiResource(
 *     normalizationContext={"groups"={"post:read"}},
 *     denormalizationContext={"groups"={"post:write"}},
 *     collectionOperations={
 *         "get",
 *         "post"={"path"="/post", "security"="is_granted('ROLE_USER')"}
 *     },
 *     itemOperations={
 *         "get"={"path"="/post/{id}"},
 *         "put"={"path"="/post/{id}", "security_post_denormalize"="is_granted('ROLE_ADMIN') or (object.author == user and previous_object.author == user)",  "security_message"="Désolé, vous n'etes pas propriétaire de ce post."},
 *         "delete"={"path"="/post/{id}", "security_post_denormalize"="is_granted('ROLE_ADMIN') or (object.author == user and previous_object.author == user)",  "security_message"="Désolé, vous n'etes pas propriétaire de ce post."}
 *     },
 *     attributes={
 *          "pagination_items_per_page"=9
 *     }
 * )
 * @ApiFilter(OrderFilter::class, properties={"createdAt", "id": "exact"}, arguments={"orderParameterName"="order"})
 * @ApiFilter(SearchFilter::class, properties={"category","author.id": "exact", "title": "partial", "content": "partial"})
 * @ORM\Entity(repositoryClass=PostRepository::class)
 */
class Post
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"post:read", "user:read", "response:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"post:read", "post:write", "user:read", "response:read"})
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min=2,
     *     max=255,
     *     maxMessage="Titre de votre post en 255 characteres maximum"
     * )
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"post:read", "response:read"})
     */
    private $slug;

    /**
     * @ORM\Column(type="text")
     * @Groups({"post:read", "post:write"})
     * @Assert\NotBlank()
     */
    private $content;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"post:read", "user:read"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"post:read", "user:read"})
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="posts")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"post:read"})
     */
    public $author;

    /**
     * @ORM\OneToMany(targetEntity=Response::class, mappedBy="post", orphanRemoval=true)
     * @ApiSubresource
     * @Groups({"post:read"})
     */
    private $responses;

    /**
     * @ORM\ManyToMany(targetEntity=Tag::class, inversedBy="posts", cascade="persist")
     * @Groups({"post:read", "post:write"})
     */
    private $tags;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="posts")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"post:read", "post:write", "response:read" })
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity=State::class, inversedBy="posts")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"post:read", "post:write", "response:read"})
     */
    private $state;

    public function __construct(string $title = null)
    {
        $this->title = $title;
        $this->createdAt = new \DateTimeImmutable();
        $this->responses = new ArrayCollection();
        $this->tags = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return Collection|Response[]
     */
    public function getResponse(): Collection
    {
        return $this->responses;
    }

    public function addResponse(Response $response): self
    {
        if (!$this->responses->contains($response)) {
            $this->responses[] = $response;
            $response->setPost($this);
        }

        return $this;
    }

    public function removeResponse(Response $response): self
    {
        if ($this->responses->removeElement($response)) {
            // set the owning side to null (unless already changed)
            if ($response->getPost() === $this) {
                $response->setPost(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        $this->tags->removeElement($tag);

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getState(): ?State
    {
        return $this->state;
    }

    public function setState(?State $state): self
    {
        $this->state = $state;

        return $this;
    }

}
