<p align="center">
  <h3 align="center">Forum API</h3>

  <p>
    <strong>Projet PHP-REST-JS : Nous avons chosi de mettre en place le projet du forum.</strong>
    <br />
    Ce dernier est décomposé en 2: 
    <br />  
    - L'api qui est développé à l'aide de Symfony et API Platform.
    <br />
    - Le front en Single Page Application qui est développé en ReactJS.
    <br/> 
    - L'authentification à l'api se fait grâce à un token JWT.
    <br />
    <!-- <a href="https://gitlab.com/meunier.jeason/forumapi"><strong>Explore the docs »</strong></a> -->
    <br />
    <br />
    <p align="center">
        <a href="https://boom-ker.live/">View Demo</a>
        ·
        <a href="https://gitlab.com/meunier.jeason/forumapi/issues">Report Bug</a>
        ·
        <a href="https://gitlab.com/meunier.jeason/forumapi/issues">Request Feature</a>
    </p>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Sommaire</h2></summary>
  <ol>
    <li>
      <a href="#a-propos-du-projet">A propos du projet</a>
      <ul>
        <li><a href="#outils-utilisés">Outils utilisés</a></li>
      </ul>
    </li>
    <li>
      <a href="#démarrage">Démarrage</a>
      <ul>
        <li><a href="#pré-requis">Pré-requis</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#spécifications-techniques">Spécifications techniques</a></li>
    <li><a href="#utilisation">Utilisation</a></li>
    <li><a href="#le-front-end-du-forum">Le front-end du Forum</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## A propos du projet

<!-- [![Product Name Screen Shot][product-screenshot]](https://example.com) -->
**Attention, ce repo n'est que la partie API du projet**
Ce travail est le résultat du projet PHP-JS-REST de la licence PRO WEB & MOBILE d'Orléans:

Le but est de développer un petit forum en PHP. Ce forum comportera des catégories prédéfinies et les utilisateurs enregistrés pourront y ajouter des sujets ou ajouter des réponses aux sujets existants. 
Les fonctionnalités suivantes sont demandées :
* Un visiteur non authentifié a le droit de voir toutes les pages du forum mais pas de publier
* Un visiteur authentifié à la droit de faire un post dans une catégorie en y créant un nouveau sujet ou en répondant à un sujet existant
* Un administrateur a le droit de créer des utilisateurs et est aussi modérateur de ce forum.
* Implémentez une API JSON permettant par exemple de récupérer tous les posts d’une certaine catégorie et chaque post individuellement.
* Si possible : Réaliser l’application via une SPA (Single Page Application).
* Utilisez un thème responsive


### Outils utilisés

Pour la mise en place de l'API
* [Symfony 5](https://symfony.com/)
* [API Platform](https://api-platform.com/)
* [Lexit JWT Token](https://github.com/lexik/LexikJWTAuthenticationBundle)

Pour la mise en place du Front-End
* [ReactJS](https://fr.reactjs.org/)
* [Create-React-App](https://github.com/facebook/create-react-app)
* [Npm](https://www.npmjs.com/)


<!-- GETTING STARTED -->
## Démarrage

Afin de déployer localement une copie de cette API, merci de suivre les étapes suivantes.

### Pré-requis

Pour déployer cet API, les prérequis sont les suivants:
* Php et les modules nécessaires au lancement d'une application symfony
  ```sh
  # Installation de php7.3 sur Debian10
  sudo apt update && sudo apt -y upgrade
  sudo apt -y install php php-common
  sudo apt -y install php-cli php-fpm php-json php-pdo php-mysql php-zip php-gd  php-mbstring php-curl php-xml php-pear php-bcmath
  ```
* Composer  
  ```sh
  #Ces commandes permettent l'installation de composer au 15/01/2021
  php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
  php -r "if (hash_file('sha384', 'composer-setup.php') === '756890a4488ce9024fc62c56153228907f1545c228516cbf63f885e036d37e9a59d27d63f46af1d4d07ee0f76181c7d3') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
  php composer-setup.php --install-dir=/usr/local/bin --filename=composer
  # Si l'installation est ok:
  php -r "unlink('composer-setup.php');"
  ```
* Un SGBD (Mysql/MariaDB/SQLite...) 
_(Nous avons utilisés MariaDB en version 10.3.25 durant notre développement)_
  ```sh
  #Installation de MariaDB sur Debian 10
  sudo apt install -y mariadb-server mariadb-client
  sudo mysql_secure_installation
  #Répondre aux questions 
  #Change the root password? [Y/n] y
  #Remove anonymous users? [Y/n] y
  #Disallow root login remotely? [Y/n] y
  #Remove test database and access to it? [Y/n] y
  #Reload privilege tables now? [Y/n] y

  #Vous pouvez maintenant créer un utilisateur dans mysql
  ```
* OpenSSL (Afin de génerer les clés pour JWT Token). OpenSSL est disponible par défaut sur Debian10
_(Dans le cadre de ce projet, nous avons fourni des clés à la racine qu'il faudra déplacer dans le dossier config/jwt. Le passphrase est: jeasondamien)_
* L'outil Symfony afin d'avoir un serveur local
  ```sh
  wget https://get.symfony.com/cli/installer -O - | bash
  # installer de manière globale sur le systeme (adapter le chemin à votre systeme)
  mv /home/user/.symfony/bin/symfony /usr/local/bin/symfony

  ```

### Installation

1. Cloner ce repo
   ```sh
   git clone https://gitlab.com/meunier.jeason/forumapi.git
   ```
2. Préparer l'installation
   Dans un premier temps, nous allons préparer JWT Token
   ```sh
   cd forumapi
   mkdir config/jwt
   
   #Génerer les clés pour l'utilisation de JWT Token
   #Si vous ne voulez pas génerer les clés, un jeu de clés est fourni a la racine du projet
   #Il vous suffit de déplacer les fichiers private.pem et public.pem dans le dossier config/jwt
   #La passphrase de ce couple de clés est: jeasondamien
   
   openssl genrsa -out config/jwt/private.pem -aes256 4096
   #Un passphrase vous sera demandé. Plus le passphrase sera complexe,
   #plus l'appli sera sécurisé

   openssl rsa -pubout -in config/jwt/private.pem -out config/jwt/public.pem
   #Vous devez confirmer le passphrase de la commande d'avant
   ```
   Ensuite, nous devons modifier le .env (situé à la racine du projet)
   ```sh
    Ajouter la configuration de connexion à la BDD sous la ligne 
    (modifier la ligne en fonction de votre BDD):
    ###< doctrine/doctrine-bundle ###
    DATABASE_URL="mysql://USER:PASSWORD@127.0.0.1:3306/forumApi?serverVersion=mariadb-10.3.25"


    Dans la partie ci-dessous, renseigner le passphrase qui vous
    a servi a générer les clés plus tot
    ###> lexik/jwt-authentication-bundle ###
    JWT_SECRET_KEY=%kernel.project_dir%/config/jwt/private.pem
    JWT_PUBLIC_KEY=%kernel.project_dir%/config/jwt/public.pem
    JWT_PASSPHRASE= MaPassPhraseAModifier
    ###< lexik/jwt-authentication-bundle ###


    Si vous ne déployer cette application en Localhost, il faudra modifier la 
    ligne suivante afin de ne pas avoir de probleme avec les CORS (ajouté votre ip et le http):
    #Dans cet exemple nous avons ajouté (http|https) |192\.168\.1\.20
    ###> nelmio/cors-bundle ###
    CORS_ALLOW_ORIGIN=^(http|https)?://(localhost|127\.0\.0\.1|192\.168\.1\.20)(:[0-9]+)?$
    ###< nelmio/cors-bundle ###
   ```

2. Installation
    Vérifier les prerequis pour symfony:
    ```sh
    composer require symfony/requirements-checker
    ```
    Si aucune erreur, on installe les Vendors:
    ```sh
    composer install
    ```
    Création de la base de donnée:
    ```sh
    php bin/console doctrine:database:create
    ```
    Insertion des données fournis:
    ```sh
    php bin/console doctrine:database:import forumApi.sql
    ```
    *Cette dernière commande est déprécié mais fonctionne. Si vous ne voulez pas l'utiliser, vous pouvez importer les données directement grâce a votre SGBD*
    
    
L'api est maitenant prête à fonctionner!

Pour lancer le serveur:
```sh
symfony serve
```

## Spécifications techniques

Voici un schéma de la base de données:
![](bdd.png)

La sécurité de notre API a était une préoccupation.
Après plusieurs jours de recherche, la solution retenu afin de sécuriser l'authentification à l'API est la suivante:
* Utilisation d'un JWT Token
* Lorsque l'on fait un appel sur la route /api/login, si les infos de connexions sont correctes:
  * Le token est coupé en deux parties: 
    * une partie comprennant le header et le paylod
    * une partie comprennant la signature
  * Les deux parties du token sont envoyés avec un setCookie au client:
    * Le cookie qui contient la signature du token est en HttpOnly (afin d'eviter les failles/attaques XSS), le client JS ne peut pas lire son contenu
    * Le cookie qui contient le header et le payload n'est pas en HttpOnly, ce qui permet à notre client JS de le lire et de récuperer des infos sur l'utilisateur
  * La sécurité des cookies peut être personalisé:
    * Pour notre projet, ils ne sont pas en "secure" car la démo n'est pas en Https
    * Le SameSite est en Lax, ce qui permet de limiter les attaques/failles CSRF (il est possible de le mettre en strict ou none)

Cette configuration permet donc d'envoyer les cookies dans la rêquete à l'API sur les routes nécessitant une authentification.

## Utilisation

Vous pouvez voir toutes les routes de l'api à l'adresse http://ip-de-l-api:8000/api.

_Vous pouvez consulter la doc de [API Platform](https://api-platform.com/) pour plus d'infos_



## Le front-end du Forum

Maintenant que l'API est en place, vous pouvez installer le Front-End du forum.
Vous le trouverez ici: [ReactForum](https://gitlab.com/meunier.jeason/reactforum)




## Développeurs/Contributeurs

Jeason Meunier / Damien Riolet

Lien du projet: [https://gitlab.com/meunier.jeason/forumapi](https://gitlab.com/meunier.jeason/forumapi)
